console.log("Starting things Up!!!\n");
var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var cors = require('cors');
var bodyParser = require('body-parser');
var ObjectId = require('mongodb').ObjectID;

var app = express();

var url = 'mongodb://localhost/TechMed';
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies



app.use(cors({origin: 'http://localhost'}));
var router = express.Router();

//route handling is done here
 /* router.get("/:id", function(req,res){
    res.send("this is a test");
    console.log("got request " + req.params.id + req.query.test);
}); */

router.get("/init/all", function(req,res){
    initAll();
    res.end();
});

router.get("/user/add", function(req,res){
    
    var email = req.query.email;
    var password = req.query.password;
    var userType = req.query.userType;
    var SQuestion = req.query.SQuestion;
    var SQuestionAnswer = req.query.SQuestionAnswer;
    
    getDbInstance(function(db,err)
    {
    
        if(!err)
        {
            console.log('Regiserting new user, succesfully connected to database.');
            //adding record to table users, well actually collection
            db.collection('users').insertOne({
                'email' : email,
                'userType' : userType,
                'SQuestion' : SQuestion,
                'SQuestionAnswer' : SQuestionAnswer,
                'password' : password
            });
        }
        else
        {
            console.log("error");
        }
 });
    res.send("works" + req.query.email + password + userType + SQuestion + SQuestionAnswer);
});

//use this to view approvals.
//router.get("/approvals/view", function(req,res){
	//var req_id = req.query.id;
    //var password = req.query.password;
    //var userType = req.query.userType;
    //var SQuestion = req.query.SQuestion;
    //var SQuestionAnswer = req.query.SQuestionAnswer;
//});

router.post('/login', function(req,res)
{
    var email = req.body.email;
    var password = req.body.password;
    var responseSent = false;
    var checkifNoUser = function()
    {
        if(!responseSent)
        {
            res.json({ status:'no_user' });
        }
    }
    
    getDbInstance(function(db,err){
    
        if(!err)
        {
            console.log('checking login information');
            db.collection('users').find({

                'email' : email

            } , function(err, cursor){
                 cursor.each(function(err,doc){
                     if(doc != null)
                     {
                            console.log(doc);
                            //var obj = JSON.parse(doc);
                            //var a = doc.toArray();
                            console.log(doc.email + doc.password);
                            if(password == doc.password)
                            {
                                responseSent = true;
                                res.json({ status:'success' , usertype: doc.userType , email: doc.email });
                            }
                            else
                            {
                                responseSent = true;
                                res.json({ status:'failed' });
                            }
                     }
                     else
                     {
                         checkifNoUser();
                     }

                 });
            });
        }
        else
        {
            console.log("error");
        }
        
    

 });
 //res.json({ status:'no_user' });
});

router.post('/addDrug', function(req,res)
{
    var drugName = req.body.drugName;
	var placeHolder = req.body.placeHolder;
    var drugType = req.body.drugType;
	var drugCardNo = req.body.drugCardNo;
	var noOfPills = req.body.noOfPills;
	var price = req.body.price;
	var qty = req.body.qty;
	
    getDbInstance(function(db,err)
    {
    
        if(!err)
        {
            
            
            db.collection('drugs').insertOne({
                'Name' : drugName,
				'placeHolder' : placeHolder,
                'drugType' : drugType,
				'drugCardNo' : drugCardNo,
				'noOfPills' : noOfPills,
				'price' : price,
				'qty' : qty
            });
        }
        else
        {
            console.log("error");
        }
	});
});

router.get('/stock/get' , function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('meds').find({},function(err,cursor)
        {
            //console.log(cursor);
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data[i] = { name: doc.name , quantity : doc.quantity, price : doc.price , id: doc._id};
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
});

router.get('/drugdispense/:id', function(req,res)
{
   
    getDbInstance(function(db,err)
    {
        
        db.collection('drugdispense').find({ patientid : parseInt(req.params.id) , dispenseStatus : 0 },function(err,cursor)
        {
            
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    console.log("not null");
                    Data[i] = { prescriptionid : doc.prescriptionid , createdate : doc.createdate, prescriptiondate : doc.prescriptiondate , _id : doc._id };
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/drugdispense/data/:id', function(req,res)
{
    getDbInstance(function(db,err)
    {
        console.log(req.params.id);
        db.collection('drugdispense').find({ "_id" : new ObjectId(req.params.id) },function(err,cursor)
        {
            
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    console.log("not null");
                    Data[i] = { data : doc.data };
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
                
                
            });
            
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/drug/get', function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('drugs').find({},function(err,cursor)
        {
            //console.log(cursor);
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data[i] = { name: doc.Name, placeHolder: doc.placeHolder,drugType: doc.drugType, drugCardNo: doc.drugCardNo, noOfPills: doc.noOfPills, price: doc.price, qty: doc.qty };
					/*  Data[i] = { placeholder: doc.PlaceHolder};  */
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/request/get', function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('requests').find({},function(err,cursor)
        {
            //console.log(cursor);
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data[i] = { drugid: doc.drugid,drugname: doc.drugname,req_quantity: doc.req_quantity,ava_quantity: doc.ava_quantity,date: doc.date,department: doc.department,status: doc.status};
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/drugdispense/dispense/:id',function(req,res)
{
    console.log(req.params.id);
    //collection.update({_id:"123"}, {$set: {author:"Jessica"}});
    getDbInstance(function(db,err)
    {
        
        db.collection('drugdispense').update({ "_id" : new ObjectId(req.params.id) } , { $set: {dispenseStatus:1}},function(err)
        {
                if(err == null)
                {
                    var data = { 'status' : 'success' };
                    res.end(JSON.stringify(data));
                }
                else
                {
                    var data = { 'status' : 'success' };
                    res.end(JSON.stringify(data));
                }
        });
        
    });
    
});

router.get('/drugdispense/search/:pid/:id',function(req,res)
{
    
    //collection.findOne({_id: doc_id}, function(err, document); 
    
     var callback = function(D)
     {
            res.end(JSON.stringify(D));   
     }
     
    getDbInstance(function(db,err)
    {
        console.log(req.params.pid);
        var Data = new Array(); // this is where we are gonna save our data for json encoding
        var i = 0;
        db.collection('drugdispense').find({ "patientid" : parseInt(req.params.pid) , "prescriptionid" : parseInt(req.params.id) },function(err,cursor)
        //db.collection('drugdispense').find({ "patientid" : parseInt(req.params.pid) }, function(err,cursor)
        {
            cursor.each(function(err,doc)
            {
                if(doc != null)
                {
                    Data[i] = doc;
                    i++;
                }
                else // this means we have iterated over all of the records
                {
                    callback(Data);    
                }
            }); 
        });     
    });
});


router.get('/batch/', function(req,res)
{

    getDbInstance(function(db,err)
    {
        db.collection('batchInfo').find({},function(err,cursor)
        {
			// this is where we are gonna save our data for json encoding
            var Data = new Array();
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data[i] = {batchNumber:doc.batchNumber,drugCategory:doc.drugCategory,drugName:doc.drugName,type:doc.type,quanity:doc.quanity,manufactureDate:doc.manufactureDate,expireDate:doc.expireDate};
                    i++;
                }
                else
                {
					//this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
				
            }
            
        });
    });
    
});


router.get('/batch/:num', function(req,res)
{
   
    getDbInstance(function(db,err)
    {
        console.log(req.params.num);
        db.collection('batchInfo').find({ batchNumber : parseInt(req.params.num)},function(err,data)
        {
            // this is where we are gonna save our data for json encoding
            var Data;
            data.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data = {batchNumber:doc.batchNumber,drugCategory:doc.drugCategory,drugName:doc.drugName,type:doc.type,quanity:doc.quanity,manufactureDate:doc.manufactureDate,expireDate:doc.expireDate};
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
		
		
		
    });
    
});


app.use("",router);

app.listen(3000,function()
{
    console.log("Server running in port 3000");
});

function initAll()
{
    console.log("inserting fake medicines");

    getDbInstance(function(db,err)
    {
         console.log('init, succesfully connected to database.');
        
         //adding record to table users, well actually collection
         db.collection('meds').insertOne({
            'name' : 'Panadol',
            'quantity' : 50,
            'price' : 500
         });

         db.collection('meds').insertOne({
            'name' : 'Vitacmin C',
            'quantity' : 100,
            'price' : 20
         });

        db.collection('meds').insertOne({
            'name' : 'Paracetamol',
            'quantity' : 500,
            'price' : 30
         });

        console.log('init,created the fake med rows');
        
        db.collection('drugdispense').insertOne({
            'patientid' : 1 ,
            'prescriptionid' : 2,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
         db.collection('drugdispense').insertOne({
            'patientid' : 2 ,
             'prescriptionid' : 3,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
         db.collection('drugdispense').insertOne({
            'patientid' : 3 ,
             'prescriptionid' : 4,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
        console.log('init,created the fake prescription');
        
        db.collection('approvals').insertOne({
        	'requestid' : 001,
        	'drugid' : 011,
        	'drugname' : 'Testdrug1011',
        	'req_quantity' : 100,
        	'ava_quantity' : 0, 
        	'date' : '2017-01-08',
        	'department' : 'IPD Pharmacy',
        	'status' : 'Pending',
        	'appr_quantity':50
        });
        
        db.collection('approvals').insertOne({
        	'requestid' : 002,
        	'drugid' : 022,
        	'drugname' : 'Testdrug1022',
        	'req_quantity' : 200,
        	'ava_quantity' : 100,
        	'date' : '2017-02-08',
        	'department' : 'OPD Pharmacy',
        	'status' : 'Pending',
        	'appr_quantity' : 100
        });
        
        db.collection('approvals').insertOne({
        	'requestid' : 003,
        	'drugid' : 033,
        	'drugname' : 'Testdrug1033',
        	'req_quantity' : 300,
        	'ava_quantity' : 200,
        	'date' : '2017-01-08',
        	'department' : 'Admin',
        	'status' : 'Pending',
        	'appr_quantity' : 200
        });
        
        console.log('init,created the fake approvals.');
        
        db.collection('requests').insertOne({
        	'requestid' : 004,
        	'drugid' : 044,
        	'drugname' : 'Testdrug1044',
        	'req_quantity' : 400,
        	'ava_quantity' : 200, 
        	'date' : '2017-03-08',
        	'department' : 'IPD Pharmacy',
        	'status' : 'Pending'
        });
        
        db.collection('requests').insertOne({
        	'requestid' : 005,
        	'drugid' : 055,
        	'drugname' : 'Testdrug1055',
        	'req_quantity' : 200,
        	'ava_quantity' : 100,
        	'date' : '2017-04-08',
        	'department' : 'OPD Pharmacy',
        	'status' : 'Pending'
        });
        
        db.collection('requests').insertOne({
        	'requestid' : 006,
        	'drugid' : 066,
        	'drugname' : 'Testdrug1066',
        	'req_quantity' : 300,
        	'ava_quantity' : 200,
        	'date' : '2017-05-08',
        	'department' : 'Admin',
        	'status' : 'Pending'
        });
        
        console.log('init,created the fake requests.');
		
		db.collection('batchInfo').insertOne({
            'batchNumber' : 1 ,
            'drugCategory' : 'Narcotics',
            'drugName' : 'Ampicillin',
            'type' : 'Cartoons',
            'quanity' : 5 ,
            'manufactureDate': '2017-06-07',
			'expireDate': '2018-06-10'
         });
		 
		 db.collection('batchInfo').insertOne({
            'batchNumber' : 2 ,
            'drugCategory' : 'Narcotics',
            'drugName' : 'Asprin',
            'type' : 'Bottles',
            'quanity' : 8 ,
            'manufactureDate': '2017-04-02',
			'expireDate': '2019-04-02'
         });
		 
		 db.collection('batchInfo').insertOne({
            'batchNumber' : 3 ,
            'drugCategory' : 'Narcotics',
            'drugName' : 'Panadol',
            'type' : 'Bottles',
            'quanity' : 32 ,
            'manufactureDate': '2017-01-02',
			'expireDate': '2019-09-02'
         });
		 
		 db.collection('batchInfo').insertOne({
            'batchNumber' : 4 ,
            'drugCategory' : 'Narcotics',
            'drugName' : 'Pain Killer',
            'type' : 'Bottles',
            'quanity' : 10 ,
            'manufactureDate': '2016-04-02',
			'expireDate': '2019-04-02'
         });
		 
		 console.log('init,created the fake batch information.');
        
    });
  
}

//this will be used to get a connection to the database
var getDbInstance = function(callback)
{
    MongoClient.connect(url, function(err, db) {
    
        if(!err)
        {
            console.log("Got db instance");
            callback(db,err);
        }
        else
        {
            console.log("error dbInstance");
        }

   });
}