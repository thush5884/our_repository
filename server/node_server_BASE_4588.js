console.log("starting things\n");
var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var cors = require('cors');
var bodyParser = require('body-parser');
var ObjectId = require('mongodb').ObjectID;

var app = express();

var url = 'mongodb://localhost/TechMed';
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies



app.use(cors({origin: 'http://localhost'}));
var router = express.Router();

//route handling is done here
 /* router.get("/:id", function(req,res){
    res.send("this is a test");
    console.log("got request " + req.params.id + req.query.test);
}); */

router.get("/init/all", function(req,res){
    initAll();
    res.end();
});

router.get("/user/add", function(req,res){
    
    var email = req.query.email;
    var password = req.query.password;
    var userType = req.query.userType;
    var SQuestion = req.query.SQuestion;
    var SQuestionAnswer = req.query.SQuestionAnswer;
    
    getDbInstance(function(db,err)
    {
    
        if(!err)
        {
            console.log('Regiserting new user, succesfully connected to database.');
            //adding record to table users, well actually collection
            db.collection('users').insertOne({
                'email' : email,
                'userType' : userType,
                'SQuestion' : SQuestion,
                'SQuestionAnswer' : SQuestionAnswer,
                'password' : password
            });
        }
        else
        {
            console.log("error");
        }
 });
    res.send("works" + req.query.email + password + userType + SQuestion + SQuestionAnswer);
});

//use this to add drugs.
//router.get("/med/add", function(req,res){
//});

router.post('/login', function(req,res)
{
    var email = req.body.email;
    var password = req.body.password;
    var responseSent = false;
    var checkifNoUser = function()
    {
        if(!responseSent)
        {
            res.json({ status:'no_user' });
        }
    }
    
    getDbInstance(function(db,err){
    
        if(!err)
        {
            console.log('checking login information');
            db.collection('users').find({

                'email' : email

            } , function(err, cursor){
                 cursor.each(function(err,doc){
                     if(doc != null)
                     {
                            console.log(doc);
                            //var obj = JSON.parse(doc);
                            //var a = doc.toArray();
                            console.log(doc.email + doc.password);
                            if(password == doc.password)
                            {
                                responseSent = true;
                                res.json({ status:'success' , usertype: doc.userType , email: doc.email });
                            }
                            else
                            {
                                responseSent = true;
                                res.json({ status:'failed' });
                            }
                     }
                     else
                     {
                         checkifNoUser();
                     }

                 });
            });
        }
        else
        {
            console.log("error");
        }
        
    

 });
 //res.json({ status:'no_user' });
});

router.get('/stock/get' , function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('meds').find({},function(err,cursor)
        {
            //console.log(cursor);
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data[i] = { name: doc.name , quantity : doc.quantity, price : doc.price , id: doc._id};
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
});

router.get('/drugdispense/:id', function(req,res)
{
   
    getDbInstance(function(db,err)
    {
        
        db.collection('drugdispense').find({ patientid : parseInt(req.params.id) , dispenseStatus : 0 },function(err,cursor)
        {
            
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    console.log("not null");
                    Data[i] = { prescriptionid : doc.prescriptionid , createdate : doc.createdate, prescriptiondate : doc.prescriptiondate , _id : doc._id };
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/drugdispense/data/:id', function(req,res)
{
    getDbInstance(function(db,err)
    {
        console.log(req.params.id);
        db.collection('drugdispense').find({ "_id" : new ObjectId(req.params.id) },function(err,cursor)
        {
            
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    console.log("not null");
                    Data[i] = { data : doc.data };
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
                
                
            });
            
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/drugdispense/dispense/:id',function(req,res)
{
    console.log(req.params.id);
    //collection.update({_id:"123"}, {$set: {author:"Jessica"}});
    getDbInstance(function(db,err)
    {
        
        db.collection('drugdispense').update({ "_id" : new ObjectId(req.params.id) } , { $set: {dispenseStatus:1}},function(err)
        {
                if(err == null)
                {
                    var data = { 'status' : 'success' };
                    res.end(JSON.stringify(data));
                }
                else
                {
                    var data = { 'status' : 'success' };
                    res.end(JSON.stringify(data));
                }
        });
        
    });
    
});

router.get('/drugdispense/search/:pid/:id',function(req,res)
{
    
    //collection.findOne({_id: doc_id}, function(err, document); 
    
     var callback = function(D)
     {
            res.end(JSON.stringify(D));   
     }
     
    getDbInstance(function(db,err)
    {
        console.log(req.params.pid);
        var Data = new Array(); // this is where we are gonna save our data for json encoding
        var i = 0;
        db.collection('drugdispense').find({ "patientid" : parseInt(req.params.pid) , "prescriptionid" : parseInt(req.params.id) },function(err,cursor)
        //db.collection('drugdispense').find({ "patientid" : parseInt(req.params.pid) }, function(err,cursor)
        {
            cursor.each(function(err,doc)
            {
                if(doc != null)
                {
                    Data[i] = doc;
                    i++;
                }
                else // this means we have iterated over all of the records
                {
                    callback(Data);    
                }
            });
            
        });
        
        
            
    });
    
       
                       
    
});


app.use("",router);

app.listen(3000,function()
{
    console.log("Server running in port 3000");
});

function initAll()
{
    console.log("inserting fake medines");

    getDbInstance(function(db,err)
    {
         console.log('init, succesfully connected to database.');
        
         //adding record to table users, well actually collection
         db.collection('meds').insertOne({
            'name' : 'Panadol',
            'quantity' : 50,
            'price' : 500
         });

         db.collection('meds').insertOne({
            'name' : 'Vitacmin C',
            'quantity' : 100,
            'price' : 20
         });

        db.collection('meds').insertOne({
            'name' : 'Paracetamol',
            'quantity' : 500,
            'price' : 30
         });

        console.log('init,created the fake med rows');
        
        db.collection('drugdispense').insertOne({
            'patientid' : 1 ,
            'prescriptionid' : 2,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
         db.collection('drugdispense').insertOne({
            'patientid' : 2 ,
             'prescriptionid' : 3,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
         db.collection('drugdispense').insertOne({
            'patientid' : 3 ,
             'prescriptionid' : 4,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
        console.log('init,created the fake prescription');
    });

            
    
}


//this will be used to get a connection to the database
var getDbInstance = function(callback)
{
    MongoClient.connect(url, function(err, db) {
    
        if(!err)
        {
            console.log("Got db instance");
            callback(db,err);
        }
        else
        {
            console.log("error dbInstance");
        }

   });
}