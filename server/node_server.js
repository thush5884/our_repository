console.log("Starting things Up!!!\n");
var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var cors = require('cors');
var bodyParser = require('body-parser');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
var ObjectId = require('mongodb').ObjectID;
//var nodemailer = require('nodemailer');
//var smtpTransport = require('nodemailer-smtp-transport');

var app = express();

var url = 'mongodb://localhost/TechMed';
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies



app.use(cors({origin: 'http://13.58.40.86'}));
var router = express.Router();

//route handling is done here
 /* router.get("/:id", function(req,res){
    res.send("this is a test");
    console.log("got request " + req.params.id + req.query.test);
}); */

router.get("/init/all", function(req,res){
    initAll();
    res.end();
});

router.get("/user/add", function(req,res){
    
    var email = req.query.email;
    var password = req.query.password;
    var userType = req.query.userType;
    var SQuestion = req.query.SQuestion;
    var SQuestionAnswer = req.query.SQuestionAnswer;
    
    getDbInstance(function(db,err)
    {
    
        if(!err)
        {
            console.log('Regiserting new user, succesfully connected to database.');
            //adding record to table users, well actually collection
            db.collection('users').insertOne({
                'email' : email,
                'userType' : userType,
                'SQuestion' : SQuestion,
                'SQuestionAnswer' : SQuestionAnswer,
                'password' : password
            });
        }
        else
        {
            console.log("error");
        }
 });
    res.send("works" + req.query.email + password + userType + SQuestion + SQuestionAnswer);
});

//Add approvals.
router.post('/approvals', function(req,res){
    var requestid = req.body.requestid;
    var drugid = req.body.drugid;
    var drugname = req.body.drugname;
    var req_quantity = req.body.req_quantity;
    var ava_quantity = req.body.ava_quantity;
    var date = req.body.date;
    var department = req.body.department;
    var status = req.body.status;
    var appr_quantity = req.body.appr_quantity;

    getDbInstance(function(db,err)
    {
        if(!err)
        {
            console.log('Adding an approval, succesfully connected to database.');
            //adding record to table approvals, well actually collection
            db.collection('approvals').insertOne({
                'requestid' : requestid,
                'drugid' : drugid,
                'drugname' : drugname,
                'req_quantity' : req_quantity,
                'ava_quantity' : ava_quantity,
                'date' : date,
                'department' : department,
                'status' : status,
                'appr_quantity' : appr_quantity
            });
        }
        else
        {
            console.log("error");
        }
    });
    res.send('success!');
});

router.post('/login', function(req,res)
{
    var email = req.body.email;
    var password = req.body.password;
    var responseSent = false;
    var checkifNoUser = function()
    {
        if(!responseSent)
        {
            res.json({ status:'no_user' });
        }
    }
    
    getDbInstance(function(db,err){
    
        if(!err)
        {
            console.log('checking login information');
            db.collection('users').find({

                'email' : email

            } , function(err, cursor){
                 cursor.each(function(err,doc){
                     if(doc != null)
                     {
                            console.log(doc);
                            //var obj = JSON.parse(doc);
                            //var a = doc.toArray();
                            console.log(doc.email + doc.password);
                            if(password == doc.password)
                            {
                                responseSent = true;
                                res.json({ status:'success' , usertype: doc.userType , email: doc.email });
                            }
                            else
                            {
                                responseSent = true;
                                res.json({ status:'failed' });
                            }
                     }
                     else
                     {
                         checkifNoUser();
                     }

                 });
            });
        }
        else
        {
            console.log("error");
        }
        
    

 });
 //res.json({ status:'no_user' });
});


router.get('/drugdispense/getAll' , function(req,res)
{
	getDbInstance(function(db,err)
    {
		db.collection('drugdispense').find({ 'dispenseStatus' : 0  }).toArray(function(err,docs)
		{
				res.end(JSON.stringify(docs));
		});
	});
});
		

router.post('/addDrug', function(req,res)
{
    var drugName = req.body.drugName;
	var placeHolder = req.body.placeHolder;
    var drugType = req.body.drugType;
	var drugCardNo = req.body.drugCardNo;
	var noOfPills = req.body.noOfPills;
	var price = req.body.price;
	var qty = req.body.qty;
	var total =req.body.total;
	
    getDbInstance(function(db,err)
    {
    
        if(!err)
        {
            
            
            db.collection('drugs').insertOne({
                'Name' : drugName,
				'placeHolder' : placeHolder,
                'drugType' : drugType,
				'drugCardNo' : drugCardNo,
				'noOfPills' : noOfPills,
				'price' : price,
				'qty' : qty,
				 'total': total
            });
        }
        else
        {
            console.log("error");
        }

	});
});



router.get('/stock/get' , function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('meds').find({},function(err,cursor)
        {
            //console.log(cursor);
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data[i] = { name: doc.name , quantity : doc.quantity, price : doc.price , id: doc._id};
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
});

router.get('/drugdispense/:id', function(req,res)
{
   
    getDbInstance(function(db,err)
    {
        
        db.collection('drugdispense').find({ patientid : parseInt(req.params.id) , dispenseStatus : 0 },function(err,cursor)
        {
            
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    console.log("not null");
                    Data[i] = { prescriptionid : doc.prescriptionid , createdate : doc.createdate, prescriptiondate : doc.prescriptiondate , _id : doc._id };
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/drugdispense/data/:id', function(req,res)
{
    getDbInstance(function(db,err)
    {
        console.log(req.params.id);
        db.collection('drugdispense').find({ "_id" : new ObjectId(req.params.id) },function(err,cursor)
        {
            
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    console.log("not null");
                    Data[i] = { data : doc.data };
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
                
                
            });
            
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/drug/get', function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('drugs').find({},function(err,cursor)
        {
            //console.log(cursor);
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data[i] = { _id:doc._id,name: doc.Name, placeHolder: doc.placeHolder,drugType: doc.drugType, drugCardNo: doc.drugCardNo, noOfPills: doc.noOfPills, price: doc.price, qty: doc.qty, total:doc.total };
					/*  Data[i] = { placeholder: doc.PlaceHolder};  */
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.get('/drug/:id', function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('drugs').find({"_id":ObjectId(req.params.id)},function(err,cursor)
        {
            //console.log(cursor);
            var Data; // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data = { _id:doc._id,name: doc.Name, placeHolder: doc.placeHolder,drugType: doc.drugType, drugCardNo: doc.drugCardNo, noOfPills: doc.noOfPills, price: doc.price, qty: doc.qty, total:doc.total };
					/*  Data[i] = { placeholder: doc.PlaceHolder};  */
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });
    });
    
});

router.put('/drug/:id',function(req,res)
{
	getDbInstance(function(db,err)
	{
		
			db.collection('drugs').update({"_id":ObjectId(req.params.id)},{$set:{Name:req.body.name,placeHolder:req.body.placeHolder,drugType:req.body.drugType,drugCardNo:req.body.drugCardNo,noOfPills:req.body.noOfPills,price:req.body.price,qty:req.body.qty,total:req.body.total}},
			function(err,data)
			{
				if(err)
				{
					console.log(err);
				}
				else{
					sendCallBack();
				}
			});
			
			var sendCallBack=function(){
				res.end("Success");
			}
	});
	
	
});

router.delete('/drug/:data',function(req,res)
{
	getDbInstance(function(db,err)
	{
		console.log(req.params.data);
		db.collection('drugs').remove({"_id":ObjectId(req.params.data)},function(err,data)
			{
				if(err)
				{
					console.log(err);
				}
				else{
					sendCallBack();
				}
			});
			
			var sendCallBack=function(){
				res.end("Success");
			}
	});
	
	
});

router.get('/request/get', function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('requests').find({},function(err,cursor)
        {
            //console.log(cursor);
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {

                if(doc != null)
                {
                    Data[i] = { requestid: doc.requestid,drugid: doc.drugid,drugname: doc.drugname,req_quantity: doc.req_quantity,ava_quantity: doc.ava_quantity,date: doc.date,department: doc.department,status: doc.status};
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }

            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }

        });
    });

});
router.get('/drugdispense/dispense/:id',function(req,res)
{
    console.log(req.params.id);
    //collection.update({_id:"123"}, {$set: {author:"Jessica"}});
    getDbInstance(function(db,err)
    {
        
        db.collection('drugdispense').update({ "_id" : new ObjectId(req.params.id) } , { $set: {dispenseStatus:1}},function(err)
        {
                if(err == null)
                {
                    var data = { 'status' : 'success' };
                    res.end(JSON.stringify(data));
                }
                else
                {
                    var data = { 'status' : 'success' };
                    res.end(JSON.stringify(data));
                }
        });
        
    });
    
});

router.get('/drugdispense/search/:pid/:id',function(req,res)
{
    
    //collection.findOne({_id: doc_id}, function(err, document); 
    
     var callback = function(D)
     {
            res.end(JSON.stringify(D));   
     }
     
    getDbInstance(function(db,err)
    {
        console.log(req.params.pid);
        var Data = new Array(); // this is where we are gonna save our data for json encoding
        var i = 0;
        db.collection('drugdispense').find({ "patientid" : parseInt(req.params.pid) , "prescriptionid" : parseInt(req.params.id) },function(err,cursor)
        //db.collection('drugdispense').find({ "patientid" : parseInt(req.params.pid) }, function(err,cursor)
        {
            cursor.each(function(err,doc)
            {
                if(doc != null)
                {
                    Data[i] = doc;
                    i++;
                }
                else // this means we have iterated over all of the records
                {
                    callback(Data);    
                }
            }); 
        });     
    });
});


router.get('/batch/', function(req,res)
{

    getDbInstance(function(db,err)
    {
        db.collection('batchInfo').find({},function(err,cursor)
        {
			// this is where we are gonna save our data for json encoding
            var Data = new Array();
            var i = 0;
            cursor.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data[i] = {batchNumber:doc.batchNumber,drugCategory:doc.drugCategory,drugName:doc.drugName,type:doc.type,quanity:doc.quanity,manufactureDate:doc.manufactureDate,expireDate:doc.expireDate};
                    i++;
                }
                else
                {
					//this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
				
            }
            
        });
    });
    
});


router.get('/batch/:num', function(req,res)
{
   
    getDbInstance(function(db,err)
    {
        db.collection('batchInfo').find({ batchNumber : req.params.num},function(err,data)
        {
            // this is where we are gonna save our data for json encoding
            var Data;
            data.each(function(err,doc)
            {
                
                if(doc != null)
                {
                    Data = {batchNumber:doc.batchNumber,drugCategory:doc.drugCategory,drugName:doc.drugName,type:doc.type,quanity:doc.quanity,manufactureDate:doc.manufactureDate,expireDate:doc.expireDate};
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
                
            });
            
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
            
        });	
		
    });
    
});

//Send Emails.
/*
router.post('/mail', function(req,res)
{
    var data = req.body;
    var date = new Date();

    const transporter = nodemailer.createTransport(smtpTransport({
        service: 'Gmail',
        auth: {
            user: 'techmed.hass@gmail.com',//Pharmacie's username
            pass: 'techmed123'//Pharmacie's password
        },
        tls: { rejectUnauthorized: false }
    }));

    //filling details.
    const mailOptions = {
        from: data.from,
        to: data.to,
        subject: data.subject,
        text: data.txt
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

    getDbInstance(function(db,err)
    {
        if(!err)
        {
            console.log('Adding an email, succesfully connected to database.');
            //adding record to table emails, well actually collection
            db.collection('emails').insertOne({
                'date': date.toDateString(),
                'time': date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(),
                'from' : data.from,
                'to' : data.to,
                'subject' : data.subject,
                'text' : data.txt
            });
        }
        else
        {
            console.log("error");
        }
    });
    res.send('success!');
});
*/

//View Send Emails
router.get('/email/get', function(req,res)
{
    getDbInstance(function(db,err)
    {
        db.collection('emails').find({},function(err,cursor)
        {
            //console.log(cursor);
            var Data = new Array(); // this is where we are gonna save our data for json encoding
            var i = 0;
            cursor.each(function(err,doc)
            {
                if(doc != null)
                {
                    Data[i] = {date: doc.date,time: doc.time,from: doc.from,to: doc.to,subject: doc.subject,text: doc.text};
                    i++;
                }
                else
                {
                    //this means we have iterated over all of the docs we got.
                    sendJsonCallBack();
                }
            });
            var sendJsonCallBack = function()
            {
                console.log(JSON.stringify(Data));
                res.end(JSON.stringify(Data));
            }
        });
    });
});

//Delete requests.
router.delete('/requests/:requestid',function (req,res) {
    getDbInstance(function(db,err)
    {
        db.collection('requests').remove({requestid : parseInt(req.params.requestid)},function(err,data)
        {
            if(err)
            {
                errorCallBack();
            }
            else
            {
                sendCallBack();
            }
        });
        var sendCallBack = function()
        {
            console.log('Request is removed Successfully');
            res.end(req.requestid);
        }
        var errorCallBack = function () {
            console.log('Request is not removed Successfully');
            res.end(req.requestid);
        }
    });
});

router.put('/batch/:num', function(req,res)
{

    getDbInstance(function(db,err)
    {
        db.collection('batchInfo').update({ batchNumber : req.body.batchNumber},{$set:{drugCategory:req.body.drugCategory,drugName:req.body.drugName,type:req.body.type,quanity:req.body.quanity,manufactureDate:req.body.manufactureDate,expireDate:req.body.expireDate}},
        function(err,res)
        {
                if(err)
                {
                    errorCallBack();
                }
                else
                {
                    sendCallBack();
                }
        });
        
    });

    var sendCallBack = function()
    {
            console.log("Batch number: "+req.body.batchNumber+" is Updated Successfuly");
            res.end("Batch number: "+req.body.batchNumber+" is Updated Successfuly");
    }
    var errorCallBack = function()
    {
            console.log("Batch number: "+req.body.batchNumber+" is not Updated Successfuly");
            res.end("Batch number: "+req.body.batchNumber+" is not Updated Successfuly");
    }

});


router.delete('/batch/:num', function(req,res)
{

		getDbInstance(function(db,err)
		{
        
			db.collection('batchInfo').remove({ batchNumber : req.params.num},function(err,data)
			{
            
				if(err){
					errorCallBack();                
				}
				else{
                                    
                    sendCallBack();
				}
                
            
			});
			
		});

        var sendCallBack = function()
        {
                    console.log("Batch number: "+req.params.num+" is Removed Successfuly");
                    res.end("Batch number: "+req.params.num+" is Removed Successfuly");
        }
        var errorCallBack = function()
        {
                    console.log("Batch number: "+req.params.num+" is not Removed Successfuly");
                    res.end("Batch number: "+req.params.num+" is not Removed Successfuly");
        }

		
});

//multers disk storage settings
var storage = multer.diskStorage({ 
        destination: function (req, file, cb) {
            cb(null, './upload/')
        },
        filename: function (req, file, cb) {
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
        }
    });

//multer settings
var upload = multer({
                storage: storage,
                //file filter
				fileFilter : function(req, file, callback) {
                    if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length-1]) === -1) {
                        return callback(new Error('Wrong extension type'));
                    }
                    callback(null, true);
                }
            }).single('file');

//API path that will upload the files
router.post('/batch/', function(req, res) {
    //Initialization
    var exceltojson; 
    upload(req,res,function(err){
        if(err){
            console.log({error_code:1,err_desc:err});
            res.redirect("http://13.58.40.86/our_repository/#/chief/addBatch");
        }
        // Multer gives us file info in req.file object
        if(!req.file){
            console.log({error_code:1,err_desc:"No file passed"});
            return;
        }
        //start convert process
        //Check the extension of the incoming file and use the appropriate module
        if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        try {
            exceltojson({
                input: req.file.path, //the same path where we uploaded our file
                output: null //since we don't need output.json
            }, function(err,result){
                if(err) {
                    console.log({error_code:1,err_desc:err, data: null});
                }
                console.log(result); 
                getDbInstance(function(db,err)
                {  
                    if(!err)
                    {            
                        db.collection('batchInfo').insert(result);
                        res.redirect("http://13.58.40.86/#/chief/addBatch");
                    }
                    else
                    {
                        console.log("error");
                    }
	            });


            });
        } catch (e){
            console.log({error_code:1,err_desc:"Corupted excel file"});
        }
    });

});


app.use("",router);

app.listen(3000,function()
{
    console.log("Server running in port 3000");
});

function initAll()
{
    console.log("inserting fake medicines");

    getDbInstance(function(db,err)
    {
         console.log('init, succesfully connected to database.');
        
         //adding record to table users, well actually collection
         db.collection('meds').insertOne({
            'name' : 'Panadol',
            'quantity' : 50,
            'price' : 500
         });

         db.collection('meds').insertOne({
            'name' : 'Vitacmin C',
            'quantity' : 100,
            'price' : 20
         });

        db.collection('meds').insertOne({
            'name' : 'Paracetamol',
            'quantity' : 500,
            'price' : 30
         });

        console.log('init,created the fake med rows');
        
        db.collection('drugdispense').insertOne({
            'patientid' : 1 ,
            'prescriptionid' : 2,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
         db.collection('drugdispense').insertOne({
            'patientid' : 2 ,
             'prescriptionid' : 3,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
         db.collection('drugdispense').insertOne({
            'patientid' : 3 ,
             'prescriptionid' : 4,
            'createdate' : '2011/6/5',
            'prescriptiondate' : '2011/6/5',
            'dispenseStatus' : 0 ,
            'data' : [{ 'drugdescription' : 'some desc' , 'dosage' : '1 pills' , 'frequency' : '2 per day' , 'period' : '1 week' , 'quanity' : 14 }]
            
         });
        
        console.log('init,created the fake prescription');

        db.collection('approvals').insertOne({
            'requestid' : 1,
            'drugid' : 11,
            'drugname' : 'Testdrug1011',
            'req_quantity' : 100,
            'ava_quantity' : 0,
            'date' : '2017-01-08',
            'department' : 'IPD Pharmacy',
            'status' : 'Pending',
            'appr_quantity':50
        });

        db.collection('approvals').insertOne({
            'requestid' : 2,
            'drugid' : 22,
            'drugname' : 'Testdrug1022',
            'req_quantity' : 200,
            'ava_quantity' : 100,
            'date' : '2017-02-08',
            'department' : 'OPD Pharmacy',
            'status' : 'Pending',
            'appr_quantity' : 100
        });

        db.collection('approvals').insertOne({
            'requestid' : 3,
            'drugid' : 33,
            'drugname' : 'Testdrug1033',
            'req_quantity' : 300,
            'ava_quantity' : 200,
            'date' : '2017-01-08',
            'department' : 'Admin',
            'status' : 'Pending',
            'appr_quantity' : 200
        });

        console.log('init,created the fake approvals.');

        db.collection('requests').insertOne({
            'requestid' : 4,
            'drugid' : 44,
            'drugname' : 'Testdrug1044',
            'req_quantity' : 400,
            'ava_quantity' : 200,
            'date' : '2017-03-08',
            'department' : 'IPD Pharmacy',
            'status' : 'Pending'
        });

        db.collection('requests').insertOne({
            'requestid' : 5,
            'drugid' : 55,
            'drugname' : 'Testdrug1055',
            'req_quantity' : 200,
            'ava_quantity' : 100,
            'date' : '2017-04-08',
            'department' : 'OPD Pharmacy',
            'status' : 'Pending'
        });

        db.collection('requests').insertOne({
            'requestid' : 6,
            'drugid' : 66,
            'drugname' : 'Testdrug1066',
            'req_quantity' : 300,
            'ava_quantity' : 200,
            'date' : '2017-05-08',
            'department' : 'Admin',
            'status' : 'Pending'
        });

        console.log('init,created the fake requests.');
		
		
        
    });
  
}

//this will be used to get a connection to the database
var getDbInstance = function(callback)
{
    MongoClient.connect(url, function(err, db) {
    
        if(!err)
        {
            console.log("Got db instance");
            callback(db,err);
        }
        else
        {
            console.log("error dbInstance");
        }

   });
}