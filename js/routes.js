myApp.config( function ($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "login/login.html"
    })
    .when("/forgot_password", {
        templateUrl : "reset_password/reset_password.html"
    })
    .when("/assistant/home", {
        templateUrl : "assistant/home.html"
    })
    .when("/chief/home", {
        templateUrl : "chief/home.html"
    })
    .when("/register", {
        templateUrl : "register_user/register.html"
    })
    .when("/chief/adddrug", {
        templateUrl : "chief/adddrug.html"
    })
	.when("/chief/drug/:id", {
        templateUrl : "chief/updateInfoDrug.html"
    })
    .when("/chief/deletedrug", {
        templateUrl : "chief/deletedrug.html"
    })
	.when("/chief/viewrequest", {
        templateUrl : "chief/viewrequest.html"
    })
	.when("/chief/dashboard", {
        templateUrl : "chief/home.html"
    })
	.when("/chief/updateDrug", {
        templateUrl : "chief/updateDrug.html"
    })
	.when("/chief/addBatch", {
        templateUrl : "chief/addBatch.html"
    })
	.when("/chief/batch/:batchNumber", {
        templateUrl : "chief/UpdateBatch.html"
    })
	.when("/chief/deleteBatch", {
        templateUrl : "chief/deleteBatch.html"
    });
});
