//login form button controller
myApp.controller('loginForm',function($scope,validateLoginData,$location,$http,$rootScope){
    
    $scope.loginClicked = function()
    {
        var email = $scope.myEmail;
        var password = $scope.myPassword;
        var checkBox = $scope.rmbrChk;
        if(validateLoginData.isValidated(email,password))
        {
           //alert("validated");
          // $location.url('/assistant/home');
           
              
           $http({
               method: 'POST',
               url: "http://13.58.40.86:3000/login",
               data: { email: email , password: password }
           }).success(function (response) {
               
               var data = JSON.parse(JSON.stringify(response));
               status = data.status;
               
               if(status == "success")
               {
                   //we only do these data if loggin is success
                   
                    $rootScope.loggedInEmail = data.email;;
                    $rootScope.loggedInUserType = data.usertype;
                    console.log(data.usertype);
                    if(data.usertype == "Chief")
                    {
                        $location.url('/chief/home');
                    }
                   
                    if(data.usertype == "Assistant")
                    {
                        $location.url('/assistant/home');
                    }
                    
               }
               else
               {
                   //username or password incorrect
                   console.log("username pass incorrect");
                   $scope.incorrectPassword = true;
               }
           });
            
            
            
        }
        else
        {
           alert("not validated");
        }
    }
});

//asssitatnt dispense Search Button
myApp.controller('DispenseSearch',function($scope,validateDispenseSearch,$http)
{
	$scope.justLoggedIn = true;
	$http.get("http://13.58.40.86:3000/drugdispense/getAll")
                .then(function(response) {
                  JsonString = JSON.stringify(response.data);
                  //alert(JsonString);
                  var JsonArray = JSON.parse(JsonString);
                  $scope.dispensDataAll = JsonArray;
                  //$scope.dispenseInnerData = FinalJsonArray;
                  //alert(response.data);
                  

            });
	
	
    $scope.DsButtonClicked = function()
    {
        var SearchTextBox = $scope.DispenseSearchText;
		$scope.justLoggedIn = false;
        if(validateDispenseSearch.isValidated(SearchTextBox))
        {
            //alert(SearchTextBox);
            $http.get("http://13.58.40.86:3000/drugdispense/" + SearchTextBox )
                .then(function(response) {
                  JsonString = JSON.stringify(response.data);
                  JsonArray = JSON.parse(JsonString);
                  //alert(JsonString);
                  $scope.dispenseData = JsonArray;
                
            });
            $scope.drugDispenseView = true;
            $scope.PrescriptionDetails = false;
        }
        else
        {
            alert("Please enter a value.");
        }
       
    }
    
    $scope.PrescriptionLinkClicked = function(obid)
    {
        $scope.PrescriptionDetails = true;
        $scope.drugDispenseView = false;
        //alert(obid);
        $http.get("http://13.58.40.86:3000/drugdispense/data/" + obid)
                .then(function(response) {
                  JsonString = JSON.stringify(response.data);
                  //alert(JsonString);
                  JsonArray = JSON.parse(JsonString);
                  var FinalJsonArray = JsonArray[0].data;
                  
                  $scope.dispenseInnerData = FinalJsonArray;
                  

            });
       $scope.presciptionID = obid;
    }
    
    $scope.prescriptionBack = function()
    {
        $scope.PrescriptionDetails = false;
        $scope.drugDispenseView = true;
    }
    
    $scope.dispence = function(obid)
    {
        //alert("init dispense" + obid); 
        $http.get("http://13.58.40.86:3000/drugdispense/dispense/" + obid)
                .then(function(response) {
                  JsonString = JSON.stringify(response.data);
                  //alert(JsonString);
                  //JSON.parse(JsonString);
                  //var FinalJsonArray = JsonArray[0];
                  
                  //$scope.dispenseInnerData = FinalJsonArray;
                  alert(response.data['status']);
                  

            });
			
			html2canvas(document.getElementById('exportThis'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Stock_Details.pdf");
            }
        });
    }
    
    $scope.nestedSearch = function()
    {
        var value = $scope.nestedSearchText;
        var pvalue = $scope.DispenseSearchText;
        
        //alert(value);
        $http.get("http://13.58.40.86:3000/drugdispense/search/" + pvalue + "/" + value)
                .then(function(response) {
                  JsonString = JSON.stringify(response.data);
                  //alert(JsonString);
                  var JsonArray = JSON.parse(JsonString);
                  $scope.dispenseData = JsonArray;
                  //$scope.dispenseInnerData = FinalJsonArray;
                  //alert(response.data);
                  

            });
    }
});

//chief stock controller
myApp.controller('cheifStock',function($scope)
{
    $scope.isPlaceOrder = false;
    $scope.searchBtnClick = function()
    {
        var searchText = $scope.SearchText;
        //alert(searchText);
    }
    
    $scope.placeOrderBtnClick = function()
    {
        //alert("place order");
        $scope.isPlaceOrder = true;
    }
    
    $scope.exportBtnCLick = function()
    {
         html2canvas(document.getElementById('exportThis'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Stock_Details.pdf");
            }
        });
    }
});

//register controller
myApp.controller('registerController', function($scope, $http , $rootScope, $location , validateRegister)
{
    
    
    $scope.PasswordMatch = function()
    {
        
        var PasswordOnce = $scope.PasswordOnce;
        var PasswordTwice = $scope.PasswordTwice;
        if(PasswordOnce == PasswordTwice)
        {
            $scope.isPassmatch = false;
        }
        else
        {
            $scope.isPassmatch = true;
        }
    }
    
    $scope.registerButton = function()
    {
        var emailText = $scope.emailText;
        var PasswordOnce = $scope.PasswordOnce;
        var PasswordTwice = $scope.PasswordTwice;
        var userType = $scope.userType;
        var SQuestionAnswer = $scope.SQuestionAnswer;
        var SQuestion = $scope.SQuestion;
        
        
        if(validateRegister.isValidated(emailText,PasswordOnce,PasswordTwice,userType,SQuestion,SQuestionAnswer))
        {
            console.log("http://13.58.40.86:3000/user/add?email=" + emailText + "&password=" + PasswordTwice + "&userType=" + userType + "&SQuestionAnswer=" + SQuestionAnswer + "&SQuestion=" + SQuestion);
            $http.get("http://13.58.40.86:3000/user/add?email=" + emailText + "&password=" + PasswordTwice + "&userType=" + userType + "&SQuestionAnswer=" + SQuestionAnswer + "&SQuestion=" + SQuestion)
                .then(function(response) {
                  alert(response.data);
                  $rootScope.IsRegistered = true;
                $location.url('/');
            });
        }
        else
        {
           alert("not validated"); 
        }
        
    }
    
});

myApp.controller('homeStockController', function($scope, $http)
{
    $http.get("http://13.58.40.86:3000/stock/get")
                .then(function(response) {
    
                    var FData = JSON.parse(JSON.stringify(response.data));
                    
                    $scope.StockMedData = FData;
                
            });
});


//Request controller.
myApp.controller('RequestController', function($scope, $http) {
    function getRequests(){
        $http.get("http://13.58.40.86:3000/request/get").then((results) => {
            var FData = JSON.parse(JSON.stringify(results.data));
            $scope.RequestData = FData;
        });
    }
    getRequests();

    $scope.editingData = [];

    for (var i = 0, len = $scope.RequestData; i < len; i++) {
        $scope.editingData[$scope.RequestData[i].requestid] = false;
    }

    $scope.approve = function (request,quantity) {
        $scope.editingData[request.requestid] = true;
        $scope.RequestId = request.requestid;
        $scope.DrugId = request.drugid;
        $scope.Drugname = request.drugname;
        $scope.Req_quantity = request.req_quantity;
        $scope.Ava_quantity = request.ava_quantity;
        $scope.Date = request.date;
        $scope.Department = request.department;
        $scope.Status = request.status;
        $scope.Quantity = quantity;

        var data = ({
            requestid: $scope.RequestId,
            drugid: $scope.DrugId,
            drugname: $scope.Drugname,
            req_quantity: $scope.Req_quantity,
            ava_quantity: $scope.Ava_quantity,
            date: $scope.Date,
            department: $scope.Department,
            status: $scope.Status,
            appr_quantity: $scope.Quantity
        });
        $http.post('http://13.58.40.86:3000/approvals', data)
            .success(function (data, status, headers, config) {
                alert('Approve Successful!');
                $http.delete('http://13.58.40.86:3000/requests/'+ $scope.RequestId)
                    .success(function (data, status, headers, config) {
                        alert('Successfully Removed!');
                        getRequests();
                    }).error(function (data, status, headers, config) {
                    alert('Remove Failed');
                    getRequests();
                });
            }).error(function (data, status, headers, config) {
            alert('Approve Failed');
        });
    };

});

//get request controller.
myApp.controller('getRequestController', function($scope, $http) {
	function getRequests(){
			$http.get("http://13.58.40.86:3000/request/get").then((results) => {
				var FData = JSON.parse(JSON.stringify(results.data));
				$scope.RequestData = FData;
			});
		}
		getRequests();
});

myApp.controller('addDrugController', function($scope, $http)
{
		$scope.addDrugButton = function(){
			var drugName = $scope.drugName;
			var placeHolder = $scope.placeHolder;
			var drugType = $scope.drugType;
			var drugCardNo = $scope.drugCardNo;
			var noOfPills = $scope.noOfPills;
			var price = $scope.price;
			var qty = $scope.qty;
			
			var total = $scope.total;
			
			
			$http({
               method: 'POST',
               url: "http://13.58.40.86:3000/addDrug",
               data: { drugName: drugName ,placeHolder: placeHolder , drugType: drugType,drugCardNo: drugCardNo ,noOfPills: noOfPills ,price: price ,qty: qty,total: total  }
           }).success(function (response) {
			   console.log("Success Drug added");
		   });
		}
		
		$scope.myFuncs=function()
		{
			$scope.total=parseFloat($scope.price)*parseInt($scope.qty);
		}
		
});

myApp.controller('getDrugController', function($scope, $http) {
	$scope.getDrugs=function(){
			$http.get("http://13.58.40.86:3000/drug/get").then((results) => {
				var FData = JSON.parse(JSON.stringify(results.data));
				$scope.DrugData = FData;
			});
		}
		
		$scope.getDrugs();
});

myApp.controller('deleteDrugController', function($scope, $http,$routeParams) {
	$scope.deleteDrugs=function(data){
			$http.delete("http://13.58.40.86:3000/drug/" + data)
            .success(function (data, status, headers) {
                $scope.getDrugs();
            });
		}
		
});

myApp.controller('updateDrugController',function($scope,$http,$routeParams){
	$scope.getUDrug=function(){
			var id=$routeParams.id;
			$http.get("http://13.58.40.86:3000/drug/"+id).then((results) => {
				var FData = JSON.parse(JSON.stringify(results.data));
				$scope.UpDrug = FData;
			});
	}
	
	$scope.getUDrug();
	
	
	$scope.upDrugInfo=function()
	{
		var id=$routeParams.id;
		$http({
			method:'PUT',
			url:"http://13.58.40.86:3000/drug/"+id,
			data:{name:$scope.UpDrug.name,placeHolder:$scope.UpDrug.placeHolder,drugType:$scope.UpDrug.drugType,drugCardNo:$scope.UpDrug.drugCardNo,noOfPills:$scope.UpDrug.noOfPills,price:$scope.UpDrug.price,qty:$scope.UpDrug.qty,total:$scope.UpDrug.total}
		}).success(function(res){
			
		});
	}
			  
});


myApp.controller('EmailController', function($scope, $http) {
    function getEmails(){
        $http.get("http://13.58.40.86:3000/email/get").then((results) => {
            var FData = JSON.parse(JSON.stringify(results.data));
            $scope.EmailData = FData;
        });
    }
    getEmails();

    $scope.sendMail = function () {
        var data = ({
            from: $scope.from,
            to: $scope.to,
            subject: $scope.subject,
            txt: $scope.txt
        });
        $http.post('http://13.58.40.86:3000/mail',data).success(function (data,status,headers,config) {
            alert('Email Sent!');
            $scope.from = '';
            $scope.to = '';
            $scope.subject = '';
            $scope.txt = '';
            getEmails();
        }).error(function (data,status,headers,config) {
            alert('Email Failed!!!');
        });
    }
});

//add approval controller.
/* myApp.controller('addApprovals', function($scope, $http)
{
		$scope.approvalbutton = function(){
			var drugName = $scope.drugName;
			var drugType = $scope.drugType;
			
			$http({
               method: 'POST',
               url: "http://13.58.40.86:3000/addDrug",
               data: { drugName: drugName , drugType: drugType }
           }).success(function (response) {
			   console.log("Success Drug added");
		   });
		}
		
		function getDrugs(){
			$http.get("http://13.58.40.86:3000/drug/get").then((results) => {
				var FData = JSON.parse(JSON.stringify(results.data));
				$scope.DrugData = FData;
			});
		}
		
		getDrugs();
}); */


myApp.controller('getBatchController', function($scope, $http) {
	$scope.getBatch=function(){
			$http.get("http://13.58.40.86:3000/batch/").then((results) => {
				var FData = JSON.parse(JSON.stringify(results.data));
				$scope.BatchData = FData;

			});
		}

         $scope.formatDate = function(date){
            var dateOut = new Date(date);
            return dateOut;
        };
		
		$scope.getBatch();
});

myApp.controller('updateBatchController',function($scope,$http,$routeParams){
	$scope.getUBatch=function(){
			var num=$routeParams.batchNumber;
			$http.get("http://13.58.40.86:3000/batch/"+num).then((results) => {
				var FData = JSON.parse(JSON.stringify(results.data));
				$scope.UpBatch = FData;

                $scope.UpBatch.manufactureDate = new Date(FData.manufactureDate);
                $scope.UpBatch.expireDate = new Date(FData.expireDate);
                $scope.UpBatch.quanity=parseInt(FData.quanity);

			});
	}
	
	$scope.getUBatch();

    $scope.upBatchData=function(num)
    {
        $http({
               method: 'PUT',
               url:"http://13.58.40.86:3000/batch/"+num,
               data: {batchNumber:$scope.UpBatch.batchNumber,drugCategory:$scope.UpBatch.drugCategory,drugName:$scope.UpBatch.drugName,type:$scope.UpBatch.type,quanity:$scope.UpBatch.quanity,manufactureDate:$scope.UpBatch.manufactureDate,expireDate:$scope.UpBatch.expireDate}
           }).success(function (response) {
               alert(response);
                $scope.getUBatch();
				$scope.getBatch();
		   });
    }
	
	$scope.checkDate=function(){
		var manuDate=new Date($scope.UpBatch.manufactureDate);
		var expDate=new Date($scope.UpBatch.expireDate);
		
		if(manuDate>expDate){
			return true;
		}
		else{
		    return false;
		}
    };

    $scope.checkMDate=function(){
		var manuDate=new Date($scope.UpBatch.manufactureDate);
		var today=new Date();

		if(manuDate>today){
			return true;
		}
		else{
			return false;
		}
	};
			  
});

myApp.controller('deleteBatchController', function($scope, $http) {
	
	$scope.deleteBatch=function(num){
			$http.delete("http://13.58.40.86:3000/batch/"+num)
			.then((results) => {
                alert(results.data);
				$scope.getBatch();
			});
			
	};			 
		
});

myApp.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
}]);